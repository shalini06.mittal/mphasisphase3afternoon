package com.mphaisis.consumer.SpringBootConsumerDemo.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.mphaisis.consumer.SpringBootConsumerDemo.Product;

@RestController
public class ProductsPage {

	@GetMapping("/fetch")
	public String fetchAllProducts()
	{
		// http://localhost:8081/products
		RestTemplate template = new RestTemplate();
		return template.getForObject("http://localhost:8081/products", String.class);
	}
	@PostMapping("/add")
	public Product insert(@RequestBody Product p)
	{
		RestTemplate template = new RestTemplate();
		Product p1 = template.postForObject("http://localhost:8081/products",
				p, Product.class);
		return p1;
	}
	
}
