package com.bank.DAO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import com.bank.model.Customer;

public class CustomerStoreItem {
	
	public static void addCustomer(ArrayList<Customer> cust) {
		ObjectOutputStream objectOutputStream=null;
		try {
			
			objectOutputStream = new ObjectOutputStream(new FileOutputStream("Customer.ser"));
			objectOutputStream.writeObject(cust);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		finally {
			try {
				objectOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
	@SuppressWarnings("unchecked")
	public static ArrayList<Customer> getCustomersStored() {
		ArrayList<Customer> list = null;
		try( FileInputStream input = new FileInputStream("Customer.ser"))
		{
				ObjectInputStream objectInputStream= new ObjectInputStream(input);
				list = (ArrayList<Customer>) objectInputStream.readObject();
		}
		catch (IOException | ClassNotFoundException e1) {
			return list;
		}
		return list;
	}


}
