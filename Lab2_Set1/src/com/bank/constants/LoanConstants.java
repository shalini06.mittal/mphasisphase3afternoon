package com.bank.constants;

public class LoanConstants {

	public static final String COLLATERAL_TYPE[] =
		{"Vehicle Registration","Home Title","Insurance Documents","Fixed Deposits"};
	public static final String LOAN_TYPE[]= {"Home","Vehicle","Education","Marriage","Hospitalization"};
	public static final String ID_TYPE[]= {"Passport", "Driving license","Aadhar Card"};
	
	public static double calculateRate(double period)
	{
		if(period < 2)
			return 0.05;
		else if(period >2 && period <5)
			return 0.06;
		else if(period >=5 && period < 8)
			return 0.08;
		else
			return 0.085;
	}
	}
