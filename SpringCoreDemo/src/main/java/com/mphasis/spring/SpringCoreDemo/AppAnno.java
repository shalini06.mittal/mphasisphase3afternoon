package com.mphasis.spring.SpringCoreDemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mphasis.spring.SpringCoreDemo.model.Author;
import com.mphasis.spring.SpringCoreDemo.model.Book;

/**
 * Hello world!
 *
 */
public class AppAnno 
{
    public static void main( String[] args )
    {
       // spring has its own application  context
    	ApplicationContext context
    	= new ClassPathXmlApplicationContext("spring-anno.xml");
    	Author author1 = (Author) context.getBean("a1");
    	System.out.println(author1);
    	
    	Book book = context.getBean(Book.class);
    	System.out.println(book);
    }
    
}
