package com.mphasis.spring.SpringCoreDemo;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.mphasis.spring.SpringCoreDemo.model.Author;
import com.mphasis.spring.SpringCoreDemo.model.CollDemo;
/**
 * 
 * Inside a new package within the root with name as singer
 * Create an interface Instrument with 1 abstract method public void play()
 * Create 2 classes Guitar and Violin that implement the Instrument interface and implement the play method.
 * Playing violin
 * Create a class Singer with following members
 * data members: sname, instrument of type Instrument
 * member method: singing() => display the message
 * Shalini is singing
 * playing guitar
 *
 */
import com.mphasis.spring.SpringCoreDemo.singer.Singer;
@Configuration // replacing the xml file
// replacing the <component-scan> tag
@ComponentScan(basePackages = {
		"demo",
		"com.mphasis.spring.SpringCoreDemo"
})
public class AppConfig {

	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext context =
				new AnnotationConfigApplicationContext(AppConfig.class);
//		Author author = context.getBean(Author.class);
//		System.out.println(author);
//		Singer singer = context.getBean(Singer.class);
//		singer.singing();
		
		CollDemo cdemo = context.getBean(CollDemo.class);
		System.out.println(cdemo.getFruits());
		System.out.println(cdemo.getCertificates());
	}
	
	@Bean
	public List<String> getFruits()
	{
		System.out.println("getting fruits...");
		return Arrays.asList("Apples","Banana","Oranges");
	}
	@Bean
	public Set<String> getCertificates()
	{
		System.out.println("getting fruits...");
		Set<String> set = new HashSet<String>();
		set.add("OCJP");
		set.add("AWS Solution Architect");
		set.add("CCNA");
		return set;
	}

}
