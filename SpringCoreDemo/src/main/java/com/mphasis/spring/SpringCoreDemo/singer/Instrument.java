package com.mphasis.spring.SpringCoreDemo.singer;

public interface Instrument {

	public void play();
}
