package com.mphasis.boot.SpringBootDemo.repo;

import org.springframework.data.repository.CrudRepository;

import com.mphasis.boot.SpringBootDemo.entity.Product;
//CrudRepository is an interface that takes care of all basic CRUD operations
// select by id, select all, delete. save or update, count
public interface ProductRepository 
extends CrudRepository<Product, Integer>{

}
