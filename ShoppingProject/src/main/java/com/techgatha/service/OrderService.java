package com.techgatha.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.techgatha.database.OrderDatabase;
import com.techgatha.model.Customer;
import com.techgatha.model.Order;

public class OrderService {

	@Autowired
	private OrderDatabase orderDatabase;
	
	public OrderService() {
		System.out.println("Order service def constructor");
	}
	
	public void insertOrder(Customer customer)
	{
		Order order = new Order();
		order.setCustomer(customer);
		order.setDate(new Date());
	}
	public List<Order> getOrders(Customer customer)
	{
		return orderDatabase.getOrder(customer);
	}
	
}
