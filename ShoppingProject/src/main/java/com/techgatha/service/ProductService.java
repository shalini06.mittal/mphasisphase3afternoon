package com.techgatha.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.techgatha.database.ProductDatabase;
import com.techgatha.model.Product;

@Service
public class ProductService {

	@Autowired
	private ProductDatabase database;

	public ProductService() {
		System.out.println("product service def const");
	}

	public List<Product> list() {
		return database.list();
	}

	public void saveOrUpdate(Product Product) {
		database.saveOrUpdate(Product);
	}

	public void delete(int id) {
		database.delete(id);
	}

	public Product get(int id) {
		return database.get(id);
	}
}
