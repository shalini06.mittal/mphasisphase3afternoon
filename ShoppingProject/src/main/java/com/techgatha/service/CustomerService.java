package com.techgatha.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.techgatha.database.CustomerDatabase;
import com.techgatha.model.Customer;

@Service
public class CustomerService {

	@Autowired
	private CustomerDatabase database;

	public CustomerService() {
		System.out.println("cust service def const");
	}

	public List<Customer> list() {
		return database.list();
	}

	public void saveOrUpdate(Customer Customer) {
		database.saveOrUpdate(Customer);
	}

	public void delete(String id) {
		database.delete(id);
	}

	public Customer get(String id) {
		return database.get(id);
	}
	public boolean login(String username, String password)
	{
		System.out.println("login servcie "+username+" "+password);
		return this.database.login(username, password);
		
	}
}
