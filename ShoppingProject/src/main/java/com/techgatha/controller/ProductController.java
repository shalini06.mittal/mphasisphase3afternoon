package com.techgatha.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.techgatha.model.Product;
import com.techgatha.service.ProductService;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

@Controller
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductService service;
	
	@RequestMapping("/{pid}")
	public String getProduct(@PathVariable int pid, Map<String, Product> map)
	{
		System.out.println("get product");
		Product product = this.service.get(pid);
		System.out.println("product "+product);
		map.put("product", product);
		return "customer";
	}
	//@RequestMapping("/")
	@GetMapping
	public String getProducts(Map<String, List<Product>> map)
	{
		System.out.println("get products");
		List<Product> products = this.service.list();
		System.out.println("products "+products);
		map.put("products", products);
		return "productslist";
	}
	@RequestMapping(method = RequestMethod.POST, path="/add")
	public String addCustomer(Product product)
	{
		System.out.println("add product");
		this.service.saveOrUpdate(product);
		return "redirect:/products";
	}
	@GetMapping("/purchase")
	public String purchaseProducts(Model map, HttpSession session, RedirectAttributes attr)
	{
		System.out.println("purchase products");
		if(session.getAttribute("username") == null)
		{
			
			attr.addFlashAttribute("message", "Please login to purchase");
			attr.addFlashAttribute("purchase", true);
			return "redirect:/";
		}
		List<Product> products = this.service.list();
		System.out.println("products "+products);
		map.addAttribute("products", products);
		return "purchase";
	}
}
