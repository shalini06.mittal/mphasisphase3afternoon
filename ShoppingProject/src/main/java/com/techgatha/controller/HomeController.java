package com.techgatha.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.techgatha.service.CustomerService;

@Controller
public class HomeController {
	
	@Autowired
	private CustomerService service;

	public HomeController() {
		System.out.println("home controller def const");
	}
	@RequestMapping("/")
	public String index()
	{
		return "index";
	}
	@RequestMapping("/register")
	public String register()
	{
		return "register";
	}
	@RequestMapping("/add")
	public String addProduct()
	{
		return "addproduct";
	}
	@RequestMapping("/logout")
	public ModelAndView logout(HttpSession session)
	{
		ModelAndView mv = new ModelAndView();
		session.removeAttribute("username");
		System.out.println("logout");
		mv.addObject("username",null);
		mv.setViewName("redirect:/");
		return mv;
	}
	@RequestMapping(path =  "/login", method = RequestMethod.POST)
	public String login(@RequestParam String username, @RequestParam String password, 
			HttpSession session, @RequestParam(defaultValue = "false") boolean purchase)
	{
		System.out.println("Login "+username);
		System.out.println("purchase "+(purchase));
		if(this.service.login(username, password)) {
			session.setAttribute("username", username);
			if(purchase)
				return "redirect:/products/purchase";
			return "redirect:/customers/"+username;
		}
		System.out.println("false");
		return "redirect:/";
	}
}
