package com.techgatha.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.techgatha.model.Customer;
import com.techgatha.service.CustomerService;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

//http://localhost:8080/ShoppingProject/customers/add
@Controller
@RequestMapping("/customers")
public class CustomerController {

	
	@Autowired
	private CustomerService service;
	
	public CustomerController() {
		System.out.println("Customer Controller");
	}
	@RequestMapping(method = RequestMethod.POST, path="/add")
	public String addCustomer(Customer customer)
	{
		
		System.out.println("add customer");
		this.service.saveOrUpdate(customer);
		return "redirect:/customers";
	}
	@RequestMapping("/{username}")
	public String getCustomer(@PathVariable String username, Map<String, Customer> map, 
			HttpSession session)
	{
		System.out.println("get customer");
		System.out.println(session.getAttribute("username"));
		if(session.getAttribute("username") == null) {
			System.out.println("session null");
			return "redirect:/";
		}
		Customer customer = this.service.get(username);
		System.out.println("customer "+customer);
		map.put("customer", customer);
		return "customer";
	}
	@GetMapping
	public String getCustomers(Map<String, List<Customer>> map)
	{
		System.out.println("get customers");
		List<Customer> Customers = this.service.list();
		System.out.println("Customers "+Customers);
		map.put("customers", Customers);
		return "customerslist";
	}
}
