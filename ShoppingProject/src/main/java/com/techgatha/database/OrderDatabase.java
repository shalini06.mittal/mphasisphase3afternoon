package com.techgatha.database;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.techgatha.model.Customer;
import com.techgatha.model.LineItem;
import com.techgatha.model.Order;

@Repository
public class OrderDatabase {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private LineItemDatabase itemDatabase;

	public OrderDatabase() {
		System.out.println("order db def const");
	}

	public OrderDatabase(SessionFactory sessionFactory) {
		System.out.println("order db param const "+sessionFactory);
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public void insert(Order Order) {
		sessionFactory.getCurrentSession().saveOrUpdate(Order);
		for(LineItem item:Order.getLineitems())
		{
			this.itemDatabase.saveOrUpdate(item);
		}
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Order> getOrder(Customer customer)
	{
		List<Order> orders = new ArrayList<Order>();
		String sql = "from Order where custid='"+customer.getCustname()+"'";
		orders = sessionFactory.getCurrentSession().createQuery(sql).list();
		for(Order order:orders)
		{
			
			order.setLineitems(this.itemDatabase.getLineItem(order.getOrderid()));
		}
		return orders;
	}

}
