package com.techgatha.database;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.techgatha.model.Product;

@Repository
public class ProductDatabase {
	
	@Autowired
    private SessionFactory sessionFactory;
	
	public ProductDatabase() {
		System.out.println("prod db def const");
	}

	public ProductDatabase(SessionFactory sessionFactory) {
		System.out.println("prod db param const "+sessionFactory);
		this.sessionFactory = sessionFactory;
	}
	
	    @SuppressWarnings("unchecked")
		@Transactional
	    public List<Product> list() {        
	        List<Product> listProduct = (List<Product>) sessionFactory.getCurrentSession().createQuery("from Product").list();
	        return listProduct;
	    }
	 
	
	    @Transactional
	    public void saveOrUpdate(Product Product) {
	        sessionFactory.getCurrentSession().saveOrUpdate(Product);
	    }
	 
	    @Transactional
	    public void delete(int id) {
	        Product ProductToDelete = new Product();
	        ProductToDelete.setPid(id);
	        sessionFactory.getCurrentSession().delete(ProductToDelete);
	    }
	 
	    @Transactional
	    public Product get(int id) {
	    	Product product = sessionFactory.getCurrentSession().get(Product.class, id);
	       if(product != null)
	    	   return  product;
	        return null;
	    }

}
