package com.techgatha.database;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.techgatha.model.Customer;

@Repository
public class CustomerDatabase {

	@Autowired
	private SessionFactory sessionFactory;

	public CustomerDatabase() {
		System.out.println("cust db def const");
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Transactional
	public boolean login(String username, String password)
	{
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Customer.class)
				.setProjection(Projections.projectionList()
						.add(Projections.property("username"), "username")
						.add(Projections.property("password"), "password"))
				.add(Restrictions.eq("username", username))
				.setResultTransformer(Transformers.aliasToBean(Customer.class));
		List list = cr.list();
		if(list.size() > 0) {
			Customer result = (Customer) list.get(0);
			System.out.println(result);

			if(result.getUsername().equals(username) && result.getPassword().equals(password))
				return true;
		}
		return false;
	}
	public CustomerDatabase(SessionFactory sessionFactory) {
		System.out.println("cust db param const "+sessionFactory);
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Customer> list() {        
		List<Customer> listCustomer = (List<Customer>) sessionFactory.getCurrentSession().createQuery("from Customer").list();
		return listCustomer;
	}

	@Transactional
	public void saveOrUpdate(Customer Customer) {
		sessionFactory.getCurrentSession().saveOrUpdate(Customer);
	}

	@Transactional
	public void delete(String id) {
		Customer CustomerToDelete = new Customer();
		CustomerToDelete.setUsername(id);
		sessionFactory.getCurrentSession().delete(CustomerToDelete);
	}

	@Transactional
	public Customer get(String id) {
		Customer customer = sessionFactory.getCurrentSession().get(Customer.class, id);
		if(customer != null)
			return  customer;
		return null;
	}

}
