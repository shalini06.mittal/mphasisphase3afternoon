package com.techgatha.database;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.techgatha.model.LineItem;

@Repository
public class LineItemDatabase {

	@Autowired
	private SessionFactory sessionFactory;

	public LineItemDatabase() {
		System.out.println("lineitem db def const");
	}

	public LineItemDatabase(SessionFactory sessionFactory) {
		System.out.println("lineitem db param const "+sessionFactory);
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public void saveOrUpdate(LineItem lineItem) {
		sessionFactory.getCurrentSession().saveOrUpdate(lineItem);
	}
	@SuppressWarnings("unchecked")
	public List<LineItem> getLineItem(int orderid)
	{
		List<LineItem> items = new ArrayList<LineItem>();
		String sql1 = "from LineItem where orderid="+orderid;
		items = sessionFactory.getCurrentSession().createQuery(sql1).list();
		return items;
	}

}
