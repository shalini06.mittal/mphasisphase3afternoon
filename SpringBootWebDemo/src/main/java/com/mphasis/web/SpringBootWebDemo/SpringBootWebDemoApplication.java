package com.mphasis.web.SpringBootWebDemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.mphasis.web.SpringBootWebDemo.entity.Product;
import com.mphasis.web.SpringBootWebDemo.service.ProductService;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;

/**
 * 1) add Spring-doc dependency
 * 2) start the spring boot web app
 * 3) open url at http://localhost:8081/swagger-ui.html => PLEASE CHECK YOUR PORT NUMBER
 */
@SpringBootApplication
public class SpringBootWebDemoApplication {
/*
 * 1) pom.xml dependencies
 * 2) webapp folder and folders and files within
 * 3) application.properties
 * 4) HomeController
 */
	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebDemoApplication.class, args);
	}

	@Autowired
	private ProductService service;
	
	@Bean
	public void products()
	{
		this.service.insertProduct(new Product(1, "Pencil", "Used for writing", 30));
		this.service.insertProduct(new Product(2, "Laptop", "Apple MAC", 800000));
		this.service.insertProduct(new Product(3, "Speaker", "Sony HD", 30000));
	}
	
	@Bean
	public OpenAPI openapi()
	{
		return new OpenAPI().info(new Info()
				.description("This is basic swagger demo for developers")
				.title("Product manegement system")
				.contact(new Contact().email("contact@techgatha.com")
						.name("Shalini Mittal")
						)
				);
	}
}
