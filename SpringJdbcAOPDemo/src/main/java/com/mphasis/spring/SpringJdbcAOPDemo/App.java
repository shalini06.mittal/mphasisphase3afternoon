package com.mphasis.spring.SpringJdbcAOPDemo;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.mphasis.spring.SpringJdbcAOPDemo.db.EmployeeDB;
import com.mphasis.spring.SpringJdbcAOPDemo.model.Employee;

/**
 * Hello world!
 *
 */
@Configuration
@ComponentScan
public class App 
{

	
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        AnnotationConfigApplicationContext context = 
        		new AnnotationConfigApplicationContext(App.class);
        EmployeeDB emp = context.getBean(EmployeeDB.class);
        
//        Employee e1 = new Employee(1, "Shalini", 38732423.23, "IT");
//        Employee e2 = new Employee(2, "Sudip", 454534534.23, "Finance");
//        Employee e3 = new Employee(3, "Shahid", 7765756.23, "IT");
//        System.out.println(emp.insertEmployee(e1));
//        System.out.println(emp.insertEmployee(e2));
//        System.out.println(emp.insertEmployee(e3));
//        System.out.println(emp.insertEmployee(e1));
//        
//        System.out.println(emp.count());
        
        System.out.println(emp.getRmployeeById(1));
        
        for(Employee e : emp.getRmployees())
        	System.out.println(e);
    }
    @Bean
    public DataSource dataSource()
    {
    	System.out.println("connecting to db");
    	DriverManagerDataSource ds = new DriverManagerDataSource();
    	ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
    	ds.setUrl("jdbc:mysql://localhost:8889/mphasis");
    	ds.setUsername("root");
    	ds.setPassword("root");
    	return ds;
    }
    @Bean
    @Autowired
    public JdbcTemplate template(DataSource ds)
    {
    	System.out.println("connecting jdbc template");
    	return new JdbcTemplate(ds);
    }
}
