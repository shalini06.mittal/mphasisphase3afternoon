package com.techgatha.loan.LoanProposalSpringBootProject.springservice;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techgatha.loan.LoanProposalSpringBootProject.model.Customer;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Loan;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.CustomerRepository;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.LoanRepository;

@Service
public class CustomerService {

	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private LoanRepository loanrepo;
	
	public CustomerService() {
		System.out.println("Customer service");
	}
	
	@Transactional
	public List<Loan> getAllLoansWithCustomerId(String email)
	{
		List<Loan> loans = this.loanrepo.findByCustomerCustomerEmailId(email);
		System.out.println("***********\n");
		System.out.println(loans);
		System.out.println("***********\n");
		return loans;
	}
	public Customer findCustomerByEmail(String email) throws Exception
	{
		Customer customer = null;
		try {

			Optional<Customer> opt = this.customerRepository.findById(email);
			if(opt.isPresent())
				customer = opt.get();
		}
		catch(IllegalArgumentException e)
		{
			throw new IllegalArgumentException("Email cannot be null, Please provide ID");
		}
		catch(Exception e)
		{
			throw new Exception(e);
		}
		System.out.println("customer from db "+customer);
		return customer;

	}

	public boolean addCustomer(Customer customer) throws Exception {

		try {
			if(this.customerRepository.findById(customer.getCustomerEmailId()).isPresent())
			{
				return false;
			}
			this.customerRepository.save(customer);
		}
		catch(IllegalArgumentException e)
		{
			throw new IllegalArgumentException("Customer cannot be null, Please provide object");
		}
		catch(Exception e)
		{
			throw new Exception(e);
		}
		return true;
	}
	public boolean updateCustomer(Customer customer) throws Exception {

		try {
			if(this.customerRepository.findById(customer.getCustomerEmailId()).isPresent())
			{
				this.customerRepository.save(customer);
				return true;
			}	
		}
		catch(IllegalArgumentException e)
		{
			throw new IllegalArgumentException("Customer cannot be null, Please provide object");
		}
		catch(Exception e)
		{
			throw new Exception(e);
		}
		return false;
	}

	
}
