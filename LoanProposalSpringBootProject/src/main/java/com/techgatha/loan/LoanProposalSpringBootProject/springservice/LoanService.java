package com.techgatha.loan.LoanProposalSpringBootProject.springservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.techgatha.loan.LoanProposalSpringBootProject.constants.LoanConstants;
import com.techgatha.loan.LoanProposalSpringBootProject.exception.EntityNotFoundException;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Collateral;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Employee;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Loan;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.CollateralRepository;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.CustomerRepository;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.EmployeeRepository;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.LoanRepository;

@Service
public class LoanService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private LoanRepository loanRepository;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private CollateralRepository collateralRepository;
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, noRollbackFor=Exception.class)
	public Loan findLoanById(String loanId) throws EntityNotFoundException
	{
		Optional<Loan> loan = loanRepository.findByLoanId(loanId);
		if(loan==null) {
			throw new EntityNotFoundException("Loan Proposal not found");
		}
		loan.ifPresent(l -> Hibernate.initialize(l.getCollaterals()));
		return loan.get();
	}
	
	
	public boolean uploadCollateral(String loanId, List<String> collateralIds) 
	{
		System.out.println(loanId);
		Optional<Loan> loan = this.loanRepository.findByLoanId(loanId);
		
		if(loan.isEmpty())
			return false;
		loan.ifPresent(l->{
			List<Collateral> collaterals = new ArrayList<Collateral>();
			collateralIds.forEach(id->{
				collaterals.add(this.collateralRepository.findById(id).get());
			});
			l.setCollaterals(collaterals);
			this.loanRepository.save(l);
		});
		
		return true;		
	}
	public Loan applyForLoan(String loanType, double loanAmount,
			double period, String email)
	{
		System.out.println("applying for loan");
		Loan loan = new Loan();
		loan.setCustomer(customerRepository.findById(email).get());
		System.out.println("loan customer");
		loan.setInterestRate(LoanConstants.calculateRate(period));
		loan.setPeriod(period);
		loan.setLoanType(loanType);
		loan.setLoanAmount(loanAmount);
		int index = (int)(Math.random()* (employeeRepository.count()));
		System.out.println("index "+index);
		List<Employee> emps = new ArrayList<>();
		this.employeeRepository.findAll().forEach(emp->emps.add(emp));
		loan.setEmployee(emps.get(index));
		loan.setLoanId("ABKB00"+(this.loanRepository.count()+1)+"_"+email);
		loanRepository.save(loan);
		System.out.println(loan);
		return loan;
	}

}
