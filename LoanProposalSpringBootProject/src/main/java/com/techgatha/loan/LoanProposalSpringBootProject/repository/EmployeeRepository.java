package com.techgatha.loan.LoanProposalSpringBootProject.repository;

import org.springframework.data.repository.CrudRepository;

import com.techgatha.loan.LoanProposalSpringBootProject.model.Customer;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee	,String>{

}
